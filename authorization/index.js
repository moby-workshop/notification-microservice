const {
    sendRequest
} = require('../http-client');
const {
    ServerError
} = require('../errors');

const authorizeUser = (...roles) => {

    return async (req, res, next) => {
        if (!req.headers.authorization) {
            throw new ServerError("Authorization Header missing!", 403);
        }

        const options = {
            url: `http://${process.env.AUTH_SERVICE}/api/token/authorizeAndReturnDetails`,
            headers: {
                'Authorization': req.headers.authorization
            }
        }
    
        const {
            userId,
            role,
            email
        } = await sendRequest(options);
        
        if (!roles.includes(role)) {
            throw new ServerError(`${role} is not allowed for this method!`, 403);
        }

        req.state = {
            userId,
            email
        }
    
        next();
    }
}


module.exports = {
    authorizeUser
};