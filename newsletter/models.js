class MailingPayload {
    constructor(bookName, author, emails) {
        this.mailingList = emails.map(e => e.to_email);
        this.book = {
            name: bookName,
            author: author
        }
    }
}

module.exports = {
    MailingPayload
}