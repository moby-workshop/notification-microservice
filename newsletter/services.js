const {
    query
} = require('../data');

const {
    ServerError
} = require('../errors');

const {
    MailingPayload
} = require('./models.js');
const { sendRequest } = require('../http-client');

const subscribe = async (userId, email) => {
    console.info(`Subscribing ${userId} with email ${email}`);

    try {
        await query('INSERT INTO newsletter (id_user, to_email) VALUES ($1, $2)', [userId, email]);
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError('Subscription already exists!', 409);
        } 
        throw e;
    }
};

const unsubscribe = async (userId) => {
    console.info(`Unsubscribing ${userId}...`);
    
    await query('DELETE FROM newsletter WHERE id_user = $1', [userId]);
};

const notify = async (bookName, author) => {
    console.info(`Sending newsletter notifications...`);

    const emails = await query('SELECT to_email FROM newsletter');

    if (emails.length === 0) {
        return;
    }
    
    const payload = new MailingPayload(bookName, author, emails);

    const options = {
        url: `http://${process.env.MAILING_SERVICE}/api/newsletter`,
        method: 'POST',
        data: payload
    };

    await sendRequest(options);
};

module.exports = {
    subscribe,
    notify,
    unsubscribe
}